package project1;

/**
 *
 * @author jeroen
 */
public class Segment {

    private final Point begin;
    private final Point end;

    //Create a segment between two points
    public Segment(Point p, Point q) {
        this.begin = p;
        this.end = q;        
    }

    public Point getBegin() {
        return begin;
    }

    public Point getEnd() {
        return end;
    }

}
