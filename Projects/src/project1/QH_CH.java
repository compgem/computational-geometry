package project1;

import java.util.ArrayList;

/**
 *
 * @author jeroen
 */
public class QH_CH extends Experiment{

    @Override
    public ArrayList<Point> startExperiment(ArrayList<Point> points) {
        initialize(points);

        Segment s = new Segment(points.get(0), points.get(points.size() - 1));
        Line l = new Line(s.getBegin(), s.getEnd());

        ArrayList<Point> upper = new ArrayList<>();
        ArrayList<Point> lower = new ArrayList<>();

        for (int i = 1; i < points.size() - 1; i++) {
            int side = predicates.sidedness(points.get(i), l);
            if (side == 1) {
                upper.add(points.get(i));
            }
            if (side == -1) {
                lower.add(points.get(i));
            }
        }

        ArrayList<Point> upperHull = new ArrayList<>();
        ArrayList<Point> lowerHull = new ArrayList<>();

        upperHull.add(s.getBegin());

        quickHull(s, upper, upperHull, true);
        quickHull(s, lower, lowerHull, false);

        predicates.sortPoints(upperHull, true);
        predicates.sortPoints(lowerHull, false);

        upperHull.add(s.getEnd());
        upperHull.addAll(lowerHull);

        return upperHull;
    }

    public void initialize(ArrayList<Point> points) {
        predicates.sortPoints(points, true);
    }

    //adds the upperhull/lowerhull of a set of points that lie above/under segment s to CH, supposing the points are sorted
    public void quickHull(Segment s, ArrayList<Point> points, ArrayList<Point> CH, boolean upperHull) {
        if (points.isEmpty()) {
            return;
        }

        //find the point with largest distance to s
        int highest = 0;
        double d = predicates.getDistance(s, points.get(0));

        for (int i = 1; i < points.size(); i++) {
            double temp = predicates.getDistance(s, points.get(i));
            if (temp > d) {
                d = temp;
                highest = i;
            }
        }

        CH.add(points.get(highest));

        ArrayList<Point> Pl = new ArrayList<>();
        ArrayList<Point> Pr = new ArrayList<>();

        Segment segmentl = new Segment(s.getBegin(), points.get(highest));
        Line linel = new Line(segmentl.getBegin(), segmentl.getEnd());
        Segment segmentr = new Segment(points.get(highest), s.getEnd());
        Line liner = new Line(segmentr.getBegin(), segmentr.getEnd());

        int upper = upperHull ? 1 : -1;

        for (int i = 0; i < highest; i++) {
            if (predicates.sidedness(points.get(i), linel) == upper) {
                Pl.add(points.get(i));
            }
        }

        for (int i = highest + 1; i < points.size(); i++) {
            if (predicates.sidedness(points.get(i), liner) == upper) {
                Pr.add(points.get(i));
            }
        }

        quickHull(segmentl, Pl, CH, upperHull);
        quickHull(segmentr, Pr, CH, upperHull);
    }

}
