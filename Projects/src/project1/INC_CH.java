package project1;

import java.util.ArrayList;

/**
 *
 * @author jeroen
 */
public class INC_CH extends Experiment {

    @Override
    public ArrayList<Point> startExperiment(ArrayList<Point> points) {
        ArrayList<Point> L_upper = new ArrayList<>(); //List for the upper hull
        ArrayList<Point> L_lower = new ArrayList<>(); //List for the lower hull
        ArrayList<Point> L = new ArrayList<>(); //List for the complete hull  

        predicates.sortPoints(points, true);

        //construct upper hull
        L_upper.add(points.get(0));
        L_upper.add(points.get(1));

        for (int i = 2; i < points.size(); i++) {
            L_upper.add(points.get(i));
            while (L_upper.size() > 2 && !predicates.hasRightTurn(L_upper.get(L_upper.size() - 3),
                    L_upper.get(L_upper.size() - 2),
                    L_upper.get(L_upper.size() - 1),
                    true)) {
                L_upper.remove(L_upper.size() - 2);
            }
        }

        //Construct lower hull
        L_lower.add(points.get(points.size() - 1));
        L_lower.add(points.get(points.size() - 2));

        for (int i = points.size() - 3; i >= 0; i--) {
            L_lower.add(points.get(i));
            while (L_lower.size() > 2 && !predicates.hasRightTurn(L_lower.get(L_lower.size() - 3),
                    L_lower.get(L_lower.size() - 2),
                    L_lower.get(L_lower.size() - 1),
                    false)) {
                L_lower.remove(L_lower.size() - 2);
            }
        }

        //remove duplicate points
        L_lower.remove(L_lower.size() - 1);
        L_lower.remove(0);

        //concatenate the lists
        L.addAll(L_upper);
        L.addAll(L_lower);

        return L;
    }

}
