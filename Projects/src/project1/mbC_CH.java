/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package project1;

import javafx.util.Pair;
import lp.Constraint;
import lp.LinearProg;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author jeroen
 */
public class mbC_CH extends Experiment{

    @Override
    public ArrayList<Point> startExperiment(ArrayList<Point> points) {
        predicates.sortPoints(points, true);

        Set<Point> hullU = new HashSet<>();
        Set<Point> hullL = new HashSet<>();

        findPartialHull(points, hullU, true);
        findPartialHull(points, hullL, false);

        ArrayList<Point> upperhull = new ArrayList<>(hullU);
        predicates.sortPoints(upperhull, true);

        ArrayList<Point> lowerhull = new ArrayList<>(hullL);
        predicates.sortPoints(lowerhull, false);

        for(int i = 1; i < lowerhull.size()-1; i++){
            upperhull.add(lowerhull.get(i));
        }

        return upperhull;
    }


    public void findPartialHull(ArrayList<Point> points, Set<Point> hull, boolean upper){
        if(points.size() > 1){
            Line line = findBridge(points, upper);
            int i = 0;
            Point start = null;
            Point end = null;
            boolean startFound = false;
            //find the first point on the line
            while(!startFound && i < points.size()){
                if(predicates.sidedness(points.get(i), line) == 0){
                    start = points.get(i);
                    startFound = true;
                }
                i++;
            }
            //find the last point on the line
            boolean lastfound = false;
            while(i < points.size()){
                if(predicates.sidedness(points.get(i), line) == 0){
                    end = points.get(i);
                    lastfound = true;
                }
                i++;
            }

            hull.add(start);
            hull.add(end);

            ArrayList<Point> Pl = new ArrayList<>();
            ArrayList<Point> Pr = new ArrayList<>();

            //add all the first points to Pl
            i = 0;
            while(i < points.size() && points.get(i).getX() <= start.getX()){
                Pl.add(points.get(i));
                i++;
            }
            //add the last points to Pr
            while(i < points.size() && points.get(i).getX() < end.getX()) i++;
            while(i < points.size()){
                Pr.add(points.get(i));
                i++;
            }

            findPartialHull(Pl, hull, upper);
            findPartialHull(Pr, hull, upper);
        }

    }

    //finds the bridge over/under the middle of the points, supposing the points are sorted
    public Line findBridge(ArrayList<Point> points, boolean upper){
        LinearProg lpSolver = new LinearProg();
        double m = (points.get(0).getX()+points.get(points.size()-1).getX())/2;

        ArrayList<Constraint> constraints = new ArrayList<>();
        for (Point point : points) {
            constraints.add(new Constraint(point.getX(), point.getY()));
        }

        Pair<Double, Double> solution;
        if(upper){
            solution = lpSolver.solve2DMin(constraints, m);
        }  else{
            solution = lpSolver.solve2DMax(constraints, m);
        }

        return new Line(solution.getKey(), solution.getValue());
    }


}
