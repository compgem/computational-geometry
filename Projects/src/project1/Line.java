package project1;

/**
 *
 * @author jeroen
 */
public class Line {

    private double a = 0;
    private double b = 0;
    private final boolean vertical;

    //create a vertical line x = a
    public Line(double a) {
        this.a = a;
        vertical = true;
    }

    //create a line y = ax + b
    public Line(double paramA, double paramB) {
        this.a = paramA;
        this.b = paramB;
        vertical = false;
    }

    //create a line through points p and q
    public Line(Point p, Point q) {
        if (q.getX() - p.getX() != 0) {
            a = (q.getY() - p.getY()) / (q.getX() - p.getX());
            b = (-a) * p.getX() + p.getY();
            vertical = false;
        } else {
            vertical = true;
            a = q.getX();
        }
    }

    public double getA() {
        return a;
    }

    public double getB() {
        return b;
    }

    public boolean isVertical() {
        return vertical;
    }
}
