package project1;

/**
 *
 * @author jeroen
 */
public class Point {

    private final double x;
    private final double y;
    private final int label;

    //Create a point with label
    public Point(double xCoord, double yCoord, int label) {
        this.x = xCoord;
        this.y = yCoord;
        this.label = label;
    }

    //Create a point without label
    public Point(double xCoord, double yCoord) {
        this.x = xCoord;
        this.y = yCoord;
        this.label = 0;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getLabel() {
        return label;
    }
}
