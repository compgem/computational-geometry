package project1;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Karen on 15/09/2016.
 */
public class Test {


    public static void main(String[] args) throws IOException {
        timeTest();
        //testAlgo(new QH_CH());
        //test1();
        //testBridge();
    }

    public static void timeTest(){
        InputGenerator inputGenerator = new InputGenerator();
        int iterations = 10;
        int[] n = {1000, 10000, 100000, 150000, 200000, 300000, 500000, 1000000, 2000000, 5000000};
        Experiment[] experiments = {new QH_CH(), new INC_CH(), new mbC_CH(), new mbC2_CH()};
        InputGenerator.Form[] shapes = {InputGenerator.Form.CIRCLE, InputGenerator.Form.TRIANGLE, InputGenerator.Form.SQUARE, InputGenerator.Form.PARABOLA};
                
        for(int i = 0; i < shapes.length; i++){
            System.out.println("Tests for " + shapes[i]);
            System.out.println("& QH & INC & MBC & MBC2");
            for(int j = 0; j < n.length; j++){
                ArrayList<Point> input = inputGenerator.input(n[j], shapes[i]);

                System.out.print(n[j]);
                for(int k = 0; k < experiments.length; k++){
                    double result = 0;
                    for(int it = 0; it < iterations; it++){
                        
                        long startTimeComputation = System.nanoTime();
                        ArrayList<Point> CH = experiments[k].startExperiment(input);
                        long endTimeComputation = System.nanoTime();

                        result += (endTimeComputation - startTimeComputation);
                    }
                    result = result/ ((double) iterations);
                    double seconds = result / 1000000000.0;
                    System.out.print(" & " + seconds);
                }
                System.out.println("");
            }
        }
    }

    public static void testAlgo(Experiment experiment){
        int[] n = {1000, 10000, 100000, 150000, 200000, 300000, 500000, 1000000, 2000000, 5000000};
        InputGenerator inputGenerator = new InputGenerator();
        int iterations = 10;
        InputGenerator.Form[] shapes = {InputGenerator.Form.CIRCLE, InputGenerator.Form.TRIANGLE, InputGenerator.Form.SQUARE, InputGenerator.Form.PARABOLA};

        System.out.println("& circle & triangle & square & parabola");
        for(int i = 0; i < n.length; i++){
            System.out.print(n[i]);
            for(int j = 0; j < shapes.length; j++){
                double result = 0;
                //int numberOfPoints = 0;
                for(int k = 0; k < iterations; k++){
                    ArrayList<Point> input = inputGenerator.input(n[i], shapes[j]);

                    long startTimeComputation = System.nanoTime();
                    ArrayList<Point> CH = experiment.startExperiment(input);
                    long endTimeComputation = System.nanoTime();
                    
                    input.clear();
                    result += (endTimeComputation - startTimeComputation);
                    //numberOfPoints += CH.size();
                }
                result = result/ ((double) iterations);
                //numberOfPoints = numberOfPoints/iterations;
                double seconds = result / 1000000000.0;
                System.out.print(" & " + seconds);
                //System.out.print(" & " + numberOfPoints);
            }
            System.out.println("");
        }
    }

    public static void testBridge(){
        InputGenerator inputGenerator = new InputGenerator();
        int n = 10;

        ArrayList<Point> points = inputGenerator.circle(n);

        mbC_CH mbC_ch = new mbC_CH();

        Predicates predicates = new Predicates();
        predicates.sortPoints(points ,true);

        Line bridge = mbC_ch.findBridge(points, true);

        ArrayList<Point> line = new ArrayList<>();
        line.add(new Point(-1, -bridge.getA() + bridge.getB()));
        line.add(new Point(1, bridge.getA() + bridge.getB()));

        drawOutput(points, line);
    }

    public static void test1() {
        InputGenerator inputGenerator = new InputGenerator();
        int n = 10000;

        try {
            Experiment experiment;

            experiment = new mbC_CH();
            
            long startTimeReadInput = System.nanoTime();
            ArrayList<Point> input = inputGenerator.circle(n);
            long endTimeReadInput = System.nanoTime();
            System.out.println("Time taken to generate input: " + (endTimeReadInput-startTimeReadInput) + " ms");

            long startTimeComputation = System.nanoTime();
            ArrayList<Point> CH = experiment.startExperiment(input);
            long endTimeComputation = System.nanoTime();
            System.out.println("Time taken for computation: " + (endTimeComputation-startTimeComputation) + " ms");

            drawOutput(input, CH);

        } catch (Exception e) {
            System.out.println("Something went wrong with the output: " + e);
        }

    }

    public static void drawOutput(ArrayList<Point> points, ArrayList<Point> hull){
        JFrame frame = new JFrame("FrameDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(600,600);

        frame.add(new Drawing(points, hull, Drawing.Form.CIRCLE));

        frame.setVisible(true);
    }

}
