package project1;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Karen on 18/09/2016.
 */
public class InputGenerator  {
    public enum Form{PARABOLA, TRIANGLE, CIRCLE, SQUARE}

    private final Random random = new Random();

    public ArrayList<Point> parabola(int n){
        ArrayList<Point> points = new ArrayList<>();

        for(int i = 1; i <= n; i++){
            double x = random.nextDouble();
            points.add(new Point(x, x*x, i));
        }

        return points;
    }

    public ArrayList<Point> square(int n){
        ArrayList<Point> points = new ArrayList<>();

        for(int i = 1; i <= n; i++){
            double x = random.nextDouble();
            double y = random.nextDouble();
            points.add(new Point(x, y, i));
        }
        return points;
    }

    public ArrayList<Point> triangle(int n){
        ArrayList<Point> points = new ArrayList<>();

        for(int i = 1; i <= n; i++){
            double r1 = random.nextDouble();
            double r2 = random.nextDouble();

            double x = (Math.sqrt(r1)*r2)*Math.sqrt(2);
            double y = Math.sqrt(r1)*(1-r2) + Math.sqrt(r1)*r2;
            points.add(new Point(x, y, i));
        }
        return points;
    }

    public ArrayList<Point> circle(int n){
        ArrayList<Point> points = new ArrayList<>();

        for(int i = 1; i <= n; i++){
            double t = 2*Math.PI*random.nextDouble();
            double u = random.nextDouble() + random.nextDouble();
            double r = u>1 ? 2-u : u;
            points.add(new Point(r*Math.cos(t), r*Math.sin(t), i));
        }
        return points;
    }

    public ArrayList<Point> input(int n, Form shape){
        switch(shape){

            case SQUARE:
                return square(n);

            case TRIANGLE:
                return triangle(n);

            case CIRCLE:
                return circle(n);

            case PARABOLA:
                return parabola(n);
        }
        return new ArrayList<>();
    }

}
