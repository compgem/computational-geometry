package project1;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.util.ArrayList;

/**
 * Created by Karen on 17/09/2016.
 */
public class Drawing extends JPanel {

    public enum Form{PARABOLA, TRIANGLE, CIRCLE, SQUARE}

    private ArrayList<Point> points;
    private ArrayList<Point> hull;
    private double scale;
    private double x;
    private double y;

    public Drawing(ArrayList<Point> points, ArrayList<Point> hull, Form form){
        this.points = points;
        this.hull = hull;

        switch(form){

            case SQUARE:
                x = 0.1;
                y = 0.1;
                scale = 400;
                break;

            case TRIANGLE:
                x = 0.05;
                y = 0.1;
                scale = 400;
                break;

            case CIRCLE:
                x = 1.3;
                y = 1.3;
                scale = 200;
                break;

            case PARABOLA:
                x = 0.1;
                y = 0.1;
                scale = 400;
                break;
        }
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        //draw the hull
        g2d.setColor(Color.RED);

        g2d.scale(1, -1);
        g2d.translate(0, -getHeight());
        
        for (int i = 0; i < hull.size()-1; i++) {
            Shape p = new Line2D.Double((hull.get(i).getX()+x)*scale, (hull.get(i).getY()+y)*scale, (hull.get(i+1).getX()+x)*scale, (hull.get(i+1).getY()+y)*scale);
            g2d.draw(p);
        }
        Shape l = new Line2D.Double((hull.get(0).getX()+x)*scale, (hull.get(0).getY()+y)*scale, (hull.get(hull.size()-1).getX()+x)*scale, (hull.get(hull.size()-1).getY()+y)*scale);
        g2d.draw(l);


        //draw the inner points
        g2d.setColor(Color.blue);
        g2d.setStroke(new BasicStroke(3));

        for (int i = 0; i < points.size(); i++) {
            Shape p = new Line2D.Double((points.get(i).getX()+x)*scale, (points.get(i).getY()+y)*scale, (points.get(i).getX()+x)*scale, (points.get(i).getY()+y)*scale);
            g2d.draw(p);
        }

        //draw the points on the hull
        g2d.setColor(Color.black);
        g2d.setStroke(new BasicStroke(5));

        for (int i = 0; i < hull.size(); i++) {
            Shape p = new Line2D.Double((hull.get(i).getX()+x)*scale, (hull.get(i).getY()+y)*scale, (hull.get(i).getX()+x)*scale, (hull.get(i).getY()+y)*scale);
            g2d.draw(p);
        }

    }

}
