package project1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Created by Karen on 15/09/2016.
 */
public class Predicates {

    static double epsilon = (new Epsilon()).epsilon;


    public void sortPoints(ArrayList<Point> points, boolean leftToRight) {
        Comparator<Point> comparator = new Comparator<Point>() {
            @Override
            public int compare(Point p1, Point p2) {
                if (p1.getX() == p2.getX()) {
                    if (p1.getY() == p2.getY()) {
                        return 0;
                    } else {
                        return p1.getY() > p2.getY() ? -1 : 1;
                    }
                } else {
                    return p1.getX() > p2.getX() ? 1 : -1;
                }
            }
        };

        if (leftToRight) {
            Collections.sort(points, comparator);
        } else {
            Collections.sort(points, Collections.reverseOrder(comparator));
        }
    }

    //returns the distance between a point and the line through a segment
    public double getDistance(Segment s, Point p) {
        double x1 = s.getBegin().getX();
        double y1 = s.getBegin().getY();
        double x2 = s.getEnd().getX();
        double y2 = s.getEnd().getY();

        double t = Math.abs((y2 - y1) * p.getX() - (x2 - x1) * p.getY() + x2 * y1 - y2 * x1);
        double n = Math.sqrt(Math.pow(y2 - y1, 2) + Math.pow(x2 - x1, 2));

        return t / n;
    }

    /*
     Point: (x, y)
     Line: (y = ax+b)
     @returns: 1 if point is above the line
     -1 if point is below the line
     0 if point is on the line
     */
    public int sidedness(Point p, Line l) {
        if (l.isVertical()) {
            return l.getA() - p.getX() < epsilon ? 0 : 1;
        }

        if(Math.abs(p.getY() - (l.getA() * p.getX() + l.getB())) < epsilon) return 0;

        if (p.getY() > (l.getA() * p.getX() + l.getB())) {
            return 1;
        } else if (p.getY() < (l.getA() * p.getX() + l.getB())) {
            return -1;
        }
        return 0;
    }

    public boolean intersectSegments(Segment seg1, Segment seg2) {
        Line line1 = new Line(seg1.getBegin(), seg1.getEnd());
        Line line2 = new Line(seg2.getBegin(), seg2.getEnd());

        return intersectSegmentLine(line1, seg2) && intersectSegmentLine(line2, seg1);
    }

    public boolean intersectSegmentLine(Line l, Segment s) {
        int side1 = sidedness(s.getBegin(), l);
        int side2 = sidedness(s.getEnd(), l);
        return side1 != side2 || side1 == 0 || side2 == 0;
    }

    public Point findIntersection(Segment s1, Segment s2) {
        if (!intersectSegments(s1, s2)) {
            return null;
        }

        double x1 = s1.getBegin().getX();
        double x2 = s1.getEnd().getX();
        double x3 = s2.getBegin().getX();
        double x4 = s2.getEnd().getX();

        double y1 = s1.getBegin().getY();
        double y2 = s1.getEnd().getY();
        double y3 = s2.getBegin().getY();
        double y4 = s2.getEnd().getY();

        if ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4) == 0) {
            return null;
        }

        double u = ((x1 * y2 - y1 * x2) * (x3 - x4) - (x1 - x2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));
        double v = ((x1 * y2 - y1 * x2) * (y3 - y4) - (y1 - y2) * (x3 * y4 - y3 * x4)) / ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4));

        Point p = new Point(u, v);
        return p;
    }

    public boolean hasRightTurn(Point p, Point q, Point r, boolean isUpper) {
        Line l = new Line(p, q);

        if (sidedness(r, l) == 0) {
            return false;
        }

        return isUpper ? sidedness(r, l) == -1 : sidedness(r, l) == 1;
    }
}
