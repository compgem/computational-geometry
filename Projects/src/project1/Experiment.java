package project1;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

/**
 *
 * @author jeroen
 */
public abstract class Experiment {

    Predicates predicates = new Predicates();

    /**
     * Read input from file
     *
     * @param filePath
     * @return The processed input
     * @throws java.io.IOException
     */
    public ArrayList<Point> readInput(String filePath) throws IOException {
        ArrayList<Point> points = new ArrayList<>();

        int label = 1;

        for (String line : Files.readAllLines(Paths.get(filePath))) {
            if (line.startsWith(" ")) {
                break;
            }

            int i = line.indexOf(" ");
            int x = Integer.parseInt(line.substring(0, i));
            int y = Integer.parseInt(line.substring(i + 1));

            points.add(new Point(x, y, label));

            label++;
        }

        return points;
    }

    /**
     * Start the experiment
     *
     * @param points
     * @return A list of point labels of points in the Convex Hull in correct
     * order
     */
    public abstract ArrayList<Point> startExperiment(ArrayList<Point> points);

    /**
     * Write the output
     *
     * @param output
     * @param fileName
     * @throws java.io.FileNotFoundException
     * @throws java.io.UnsupportedEncodingException
     */
    public void writeOutput(ArrayList<Point> output, String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        try (PrintWriter writer = new PrintWriter(fileName, "UTF-8")) {
            output.stream().forEach((p) -> {
                writer.println(p.getLabel());
            });
        }
    }

}
