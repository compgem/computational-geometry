package project1;

import java.util.ArrayList;
import java.util.Set;

/**
 *
 * @author jeroen
 */
public class mbC2_CH extends mbC_CH {


    @Override
    public void findPartialHull(ArrayList<Point> points, Set<Point> hull, boolean upper){
        if(points.size() > 1){
            Line pruneLine = new Line(points.get(0), points.get(points.size()-1));
            Line line = findBridge(points, upper);
            int i = 0;
            Point start = null;
            Point end = null;
            boolean startFound = false;
            //find the first point on the line
            while(!startFound && i < points.size()){
                if(predicates.sidedness(points.get(i), line) == 0){
                    start = points.get(i);
                    startFound = true;
                }
                i++;
            }
            //find the last point on the line
            while(i < points.size()){
                if(predicates.sidedness(points.get(i), line) == 0){
                    end = points.get(i);
                }
                i++;
            }

            hull.add(start);
            hull.add(end);

            ArrayList<Point> Pl = new ArrayList<>();
            ArrayList<Point> Pr = new ArrayList<>();

            //add all the first points to Pl
            i = 0;
            int prune = upper ? -1 : 1;
            while(i < points.size() && points.get(i).getX() <= start.getX()){
                if(predicates.sidedness(points.get(i), pruneLine) != prune){
                    Pl.add(points.get(i));
                }
                i++;
            }
            //add the last points to Pr
            while(i < points.size() && points.get(i).getX() < end.getX()) i++;
            while(i < points.size()){
                if(predicates.sidedness(points.get(i), pruneLine) != prune){
                    Pr.add(points.get(i));
                }
                i++;
            }

            findPartialHull(Pl, hull, upper);
            findPartialHull(Pr, hull, upper);
        }
    }

}
