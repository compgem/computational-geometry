package project1;

import javax.swing.*;
import java.io.IOException;
import java.util.ArrayList;

/**
 *
 * @author jeroen
 */
public class Main {

    public static void main(String[] args) throws IOException {
        //change the used algorithm here 
        //*****************************
        Experiment e = new QH_CH(); 
        //*****************************
        ArrayList<Point> pts;
        ArrayList<Point> CH;

        long startTimeReadInput = System.nanoTime();
        
        //change location of input file here
        //*****************************
        pts = e.readInput("C:\\Users\\jeroe\\Documents\\NetBeansProjects\\computational-geometry\\Projects\\src\\project1\\ch1"); 
        //*****************************
        
        long endTimeReadInput = System.nanoTime();
        System.out.println("Time taken to read input: " + (endTimeReadInput-startTimeReadInput) + " ms");
        
        long startTimeComputation = System.nanoTime();
        CH = e.startExperiment(pts);
        long endTimeComputation = System.nanoTime();
        System.out.println("Time taken for computation: " + (endTimeComputation-startTimeComputation) + " ms");
        drawOutput(pts, CH);

        //change location of output here
        //*****************************
        e.writeOutput(CH, "output.txt"); 
        //*****************************
    }

    public static void drawOutput(ArrayList<Point> points, ArrayList<Point> hull) {
        JFrame frame = new JFrame("FrameDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(500, 500);

        frame.add(new Drawing(points, hull, Drawing.Form.CIRCLE));

        frame.setVisible(true);
    }

}
