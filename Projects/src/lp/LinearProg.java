package lp;

import javafx.util.Pair;
import project1.Epsilon;
import project1.Line;
import project1.Point;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by Karen on 21/09/2016.
 */
public class LinearProg {

    double epsilon = (new Epsilon()).epsilon;


    public double solve1DMin(ArrayList<Constraint> constraints, double m){
        double xmin = -Double.MAX_VALUE;
        double xmax = Double.MAX_VALUE;

        for(int i = 0; i < constraints.size(); i++){
            Constraint c = constraints.get(i);

            if(c.getA() == 0 && c.getB() > 0){
                System.out.println("ERROR: fault in constrains.");
                return 0;
            }

            if(c.getA() < 0){
                xmax = Math.min(xmax, c.getB()/c.getA());
            }

            if(c.getA() > 0){
                xmin = Math.max(xmin, c.getB()/c.getA());
            }
        }
        if(xmax < xmin && Math.abs(xmin - xmax) > epsilon){
            System.out.println("ERROR: No solution possible in LP.");
            return 0;
        }
        return m < 0 ? xmax : xmin;
    }

    public double solve1DMax(ArrayList<Constraint> constraints, double m){
        double xmin = -Double.MAX_VALUE;
        double xmax = Double.MAX_VALUE;

        for (Constraint c : constraints) {
            if(c.getA() < 0){
                xmin = Math.max(xmin, c.getB()/c.getA());
            }

            if(c.getA() > 0){
                xmax = Math.min(xmax, c.getB()/c.getA());
            }
        }
        if(xmax < xmin && Math.abs(xmin - xmax) > epsilon){
            System.out.println("ERROR: No solution possible in LP.");
            return 0;
        }
        return m < 0 ? xmin : xmax;
    }

    public Pair<Double, Double> solve2DMin(ArrayList<Constraint> constraints, double m){
        //intialize a and b as a line through the first and last points
        Line line = new Line(new Point(constraints.get(0).getA(), constraints.get(0).getB()),
                new Point(constraints.get(constraints.size()-1).getA(), constraints.get(constraints.size()-1).getB()));
        Pair<Double, Double> p = new Pair(line.getA(),line.getB());


        Collections.shuffle(constraints);

        for(int i = 0; i < constraints.size(); i++){
            Constraint constr = constraints.get(i);
            if(((double)p.getValue()) < -constr.getA()*((double)p.getKey()) + constr.getB()
                    && Math.abs(-constr.getA()*p.getKey() + constr.getB() - p.getValue()) > epsilon){
                ArrayList<Constraint> newConst = new ArrayList<>();
                for(int j = 0; j < i; j++){
                    newConst.add(new Constraint(constraints.get(j).getA() - constr.getA(), constraints.get(j).getB() - constr.getB()));
                }
                double x = solve1DMin(newConst, m - constr.getA());
                p = new Pair(x, -constr.getA()*x + constr.getB());
            }
        }
        return p;
    }

    public Pair<Double, Double> solve2DMax(ArrayList<Constraint> constraints, double m){
        //intialize a and b as a line through the first and last points
        Line line = new Line(new Point(constraints.get(0).getA(), constraints.get(0).getB()),
                new Point(constraints.get(constraints.size()-1).getA(), constraints.get(constraints.size()-1).getB()));
        Pair<Double, Double> p = new Pair(line.getA(),line.getB());

        Collections.shuffle(constraints);


        for(int i = 0; i < constraints.size(); i++){
            Constraint constr = constraints.get(i);
            if(p.getValue() > -constr.getA()*p.getKey() + constr.getB()
                    && Math.abs(-constr.getA()*p.getKey() + constr.getB() - p.getValue()) > epsilon){
                ArrayList<Constraint> newConst = new ArrayList<>();
                for(int j = 0; j < i; j++){
                    newConst.add(new Constraint(constraints.get(j).getA() - constr.getA(), constraints.get(j).getB() - constr.getB()));
                }
                double x = solve1DMax(newConst, m - constr.getA());
                p = new Pair(x, -constr.getA()*x + constr.getB());
            }
        }
        return p;
    }
}
