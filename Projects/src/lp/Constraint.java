package lp;

/**
 * Created by Karen on 21/09/2016.
 */
public class Constraint {

    //the constraint means b <= a*x in 1D and b <= a*x + y in 2D
    private final double a;
    private final double b;

    public Constraint(double a, double b){
        this.a = a;
        this.b = b;
    }

    public double getA(){
        return a;
    }

    public double getB(){
        return b;
    }


}
