package project2.prioritySearchTree;

import project2.Point;

/**
 *
 * @author jeroen
 */
public class PSTNode {
    private PSTNode left;
    private PSTNode right;
    private Point point;
    private double YMid;
    
    public PSTNode() {
        this.left = null;
        this.right = null;
        this.point = null;
    }
    
    public boolean isLeaf(){
        return left == null && right == null;
    }
    
    public boolean isEmptyLeaf(){
        return isLeaf() && point == null;
    }
    
    public void setPoint(Point p){
        this.point = p;
    }
    
    public Point getPoint(){
        return point;
    }
    
    public void setYMid(double YMid){
        this.YMid = YMid;
    }
    
    public double getYMid(){
        return YMid;
    }
    public void setLeft(PSTNode left) {
        this.left = left;
    }

    public void setRight(PSTNode right) {
        this.right = right;
    }
    
    public PSTNode getLeft(){
        return left;
    }
    
    public PSTNode getRight(){
        return right;
    }

    public int size(){
        int i = 1;
        if(left != null) i += left.size();
        if(right != null) i += right.size();
        return i;
    }
}
