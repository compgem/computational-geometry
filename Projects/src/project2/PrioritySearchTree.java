package project2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import project2.prioritySearchTree.PSTNode;

/**
 *
 * @author jeroen
 */
public class PrioritySearchTree {
    private PSTNode root;
    
    public PrioritySearchTree(List<Point> points, boolean isLeft){
        root = buildTree(points, isLeft);
    }
    
    public PSTNode buildTree(List<Point> points, boolean isLeft) {
        if(!points.isEmpty()){
            Point PMin = points.get(0);
            int minIndex = 0;
            double minX = PMin.getX();
            for(int i = 1; i < points.size(); i++){
                if(isLeft){
                    if(points.get(i).getX() < minX){
                        minIndex = i;
                        minX = points.get(i).getX();
                    } 
                } else{
                    if(points.get(i).getX() > minX){
                        minIndex = i;
                        minX = points.get(i).getX();
                    }
                }                
            }


            PSTNode node = new PSTNode();
            node.setPoint(points.get(minIndex));
            points.remove(minIndex);

            if(!points.isEmpty()){
                List<Double> findMed = new ArrayList<>();
                for(Point p : points){
                    findMed.add(p.getY());
                }

                Collections.sort(findMed);
                double YMid = findMed.get(findMed.size()/2);

                /*double maxY = points.get(0).getY();
                double minY = points.get(0).getY();

                for(Point p: points){
                    if(p.getY() > maxY){
                        maxY = p.getY();
                    }
                    if(p.getY() < minY){
                        minY = p.getY();
                    }
                }
                double YMid = (maxY + minY) / 2;//computeMedian(points);
            */
                List<Point> PBelow = new ArrayList<>();
                List<Point> PAbove = new ArrayList<>();

                for(Point p: points){
                    if(p.getY() <= YMid){ //The book says this should be only <, but I think it should be <=
                        PBelow.add(p);
                    }else {
                        PAbove.add(p);
                    }
                }
                                
                node.setYMid(YMid);

                if(!PBelow.isEmpty()) node.setLeft(buildTree(PBelow, isLeft));
                if(!PAbove.isEmpty()) node.setRight(buildTree(PAbove, isLeft));
            }
            
            return node;
        }        
       
        return null;
    }
    
    public double computeMedian(List<Point> points) {
        List<Double> endpoints = new ArrayList<>();        
               
        for(Point i: points){
            endpoints.add(i.getY());
        } 
        
        Collections.sort(endpoints);
        double median;
        if (endpoints.size()%2 == 0){
            median = (endpoints.get(endpoints.size()/2) + endpoints.get(endpoints.size()/2 - 1))/2;            
        }            
        else{
            median = endpoints.get((endpoints.size()-1)/2);
        }
        
        return median;            
    }
    
    public List<Point> query(double x, double y1, double y2, boolean isLeft) {
        List<Point> result = new ArrayList<>();
        
        queryPST(result, root, x, y1, y2, isLeft);
        
        return result;
    }

    public int queryPSTNaive(double x, double y1, double y2, boolean isLeft){
        return queryPSTNaive(root, x, y1, y2, isLeft);
    }

    public int queryPSTNaive(PSTNode node, double x, double y1, double y2, boolean isLeft){
        int i = 0;
        Point p = node.getPoint();
        if(isLeft){
            if(p != null && (p.getX() <= x) && (p.getY() >= y1) && (p.getY() <= y2)) i++;
        } else{
            if(p != null && (p.getX() >= x) && (p.getY() >= y1) && (p.getY() <= y2)) i++;
        }
        if(node.getLeft() != null) i += queryPSTNaive(node.getLeft(), x, y1, y2, isLeft);
        if(node.getRight() != null) i += queryPSTNaive(node.getRight(), x, y1, y2, isLeft);
        return i;
    }
    
    public List<Point> queryPST(List<Point> result, PSTNode node, double x, double y1, double y2, boolean isLeft) {
        if(node == null) return result;

        Point p = node.getPoint();
        double YMid = node.getYMid();

        if (isLeft) {
            if (node.isLeaf()) {
                if (!node.isEmptyLeaf() && (p.getX() <= x) && (p.getY() >= y1) && (p.getY() <= y2)) {
                    result.add(p);
                }
            } else {
                //query algo for range  (−∞, x] × [y1, y2]
                if (p.getX() > x) {
                    return result;
                }
                if ((p.getX() <= x) && (p.getY() >= y1) && (p.getY() <= y2)) {
                    result.add(p);
                }

                if (YMid >= y2) {
                    queryPST(result, node.getLeft(), x, y1, y2, isLeft);
                } else if (YMid < y1) {
                    queryPST(result, node.getRight(), x, y1, y2, isLeft);
                } else {
                    queryPST(result, node.getLeft(), x, y1, y2, isLeft);
                    queryPST(result, node.getRight(), x, y1, y2, isLeft);
                }
            }
        } else {
            if (node.isLeaf()) {
                if (!node.isEmptyLeaf() && (p.getX() >= x) && (p.getY() >= y1) && (p.getY() <= y2)) {
                    result.add(p);
                }
            } else {
                //query algo for range [x, ∞) × [y1, y2]
                if (p.getX() < x) {
                    return result;
                }
                if ((p.getX() >= x) && (p.getY() >= y1) && (p.getY() <= y2)) {
                    result.add(p);
                }

                if (YMid >= y2) {
                    queryPST(result, node.getLeft(), x, y1, y2, isLeft);
                } else if (YMid < y1) {
                    queryPST(result, node.getRight(), x, y1, y2, isLeft);
                } else {
                    queryPST(result, node.getLeft(), x, y1, y2, isLeft);
                    queryPST(result, node.getRight(), x, y1, y2, isLeft);
                }
            }
        }

        return result;
    }

    public int size(){
        if(root != null){
            return root.size();
        }else{
            return 0;
        }        
    }
    
}
