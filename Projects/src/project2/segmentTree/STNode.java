package project2.segmentTree;

import project2.Interval;
import project2.Segment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karen on 3/12/2016.
 */
public class STNode {
    private STNode left;
    private STNode right;
    private List<Segment> segments;
    private Interval interval;

    public STNode(Interval interval) {
        this.interval = interval;
        segments = new ArrayList<>();
    }

    public boolean isLeaf(){
        return left == null && right == null;
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public Interval getInterval() {
        return interval;
    }

    public STNode getLeft() {
        return left;
    }

    public STNode getRight() {
        return right;
    }

    public void setLeft(STNode left) {
        this.left = left;
    }

    public void setRight(STNode right) {
        this.right = right;
    }
}
