package project2;

import project2.kdtree.KDTNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karen on 11/12/2016.
 */
public class KDTree implements DataStructure{
    private PointsHandler handler;
    private KDTNode root;

    public KDTree(List<Segment> segments){
        handler = new PointsHandler();
        List<Point3D> points = handler.transformSegmentToPoint(segments);
        Interval inf = new Interval(-Double.MAX_VALUE, false, Double.MAX_VALUE, false);
        root = buildTree(points, 0, inf, inf, inf);
    }

    private KDTNode buildTree(List<Point3D> points, int depth, Interval x, Interval y, Interval z){
        double split = 0;
        if(points.size() == 1){
            return new KDTNode(points.get(0), x, y, z, depth);
        } else{
            handler.sort(points, depth);
            int m = (points.size()-1)/2;
            if(depth%3 == 0){
                split = points.get(m).getX();
                while(m < points.size()-1 && points.get(m).getX() == points.get(m+1).getX()) m++;
            }
            if(depth%3 == 1){
                split = points.get(m).getY();
                while(m < points.size()-1 && points.get(m).getY() == points.get(m+1).getY()) m++;
            }
            if(depth%3 == 2){
                split = points.get(m).getZ();
                while(m < points.size()-1 && points.get(m).getZ() == points.get(m+1).getZ()) m++;
            }

            List<Point3D> p1 = points.subList(0, m+1);
            List<Point3D> p2 = points.subList(m+1, points.size());

            KDTNode node = new KDTNode(null, x, y, z, depth);
            node.setSplitpoint(split);

            if(depth%3 == 0){
                if(!p1.isEmpty()){
                    Interval left = x.split(split, true);
                    node.setLeft(buildTree(p1, depth+1, left, y, z));
                }
                if(!p2.isEmpty()){
                    Interval right = x.split(split, false);
                    node.setRight(buildTree(p2, depth+1, right, y, z));
                }
            }

            if(depth%3 == 1){
                if(!p1.isEmpty()){
                    Interval left = y.split(split, true);
                    node.setLeft(buildTree(p1, depth+1, x, left, z));
                }
                if(!p2.isEmpty()){
                    Interval right = y.split(split, false);
                    node.setRight(buildTree(p2, depth+1, x, right, z));
                }
            }

            if(depth%3 == 2){
                if(!p1.isEmpty()){
                    Interval left = z.split(split, true);
                    node.setLeft(buildTree(p1, depth+1, x, y, left));
                }
                if(!p2.isEmpty()){
                    Interval right = z.split(split, false);
                    node.setRight(buildTree(p2, depth+1, x, y, right));
                }
            }

            return node;
        }
    }

    @Override
    public List<Segment> query(Segment segment) {
        Interval x = new Interval(-Double.MAX_VALUE, false, segment.getY(), true);
        Interval y = new Interval(segment.getY(), true, Double.MAX_VALUE, false);
        Interval z = new Interval(segment.getX1(), true, segment.getX2(), true);
        List<Point3D> result = new ArrayList<>();
        queryKDTree(result, root, x ,y, z);
        return handler.transformPointToSegment(result);
    }

    @Override
    public String getName() {
        return "kd-tree";
    }
    
    @Override
    public int getSize(){
        int size = 0;
        
        if(root != null){
            size = getNodeSize(root);
        }           
        
        return size;
    }
    
    private int getNodeSize(KDTNode node){
        int size = 1;               
       
        if(node.getLeft() != null){
            size += getNodeSize(node.getLeft());
        }
        if(node.getRight() != null){
            size += getNodeSize(node.getRight());
        }        
        
        return size;
    }

    public void queryKDTree(List<Point3D> result, KDTNode node, Interval x, Interval y, Interval z){
        if(node.isLeaf()){
            Point3D point = node.getPoint();
            if(x.contains(point.getX()) && y.contains(point.getY()) && z.contains(point.getZ())) result.add(point);
        } else{
            if(node.getLeft() != null && containsRegion(node.getLeft(), x, y, z)){
                reportSubtree(node.getLeft(), result);
            } else if(node.getLeft() != null && !disjunct(node.getLeft(), x, y, z)){
                queryKDTree(result, node.getLeft(), x, y, z);
            }

            if(node.getRight() != null && containsRegion(node.getRight(), x, y, z)){
                reportSubtree(node.getRight(), result);
            } else if(node.getRight() != null && !disjunct(node.getRight(), x, y, z)){
                queryKDTree(result, node.getRight(), x, y, z);
            }
        }
    }

    public boolean disjunct(KDTNode node, Interval x, Interval y, Interval z){
        return x.isDisjunct(node.getX()) || y.isDisjunct(node.getY()) || z.isDisjunct(node.getZ());
    }

    public boolean containsRegion(KDTNode node, Interval x, Interval y, Interval z){
        return x.contains(node.getX()) && y.contains(node.getY()) && z.contains(node.getZ());
    }

    public void reportSubtree(KDTNode node, List<Point3D> points){
        if(node.isLeaf()){
            points.add(node.getPoint());
        } else{
            if(node.getLeft() != null) reportSubtree(node.getLeft(), points);
            if(node.getRight() != null) reportSubtree(node.getRight(), points);
        }
    }
}
