package project2;

/**
 * Created by Karen on 3/12/2016.
 */
public class Interval {
    private double x;
    private boolean containsX;
    private double y;
    private boolean containsY;

    public Interval(double x, boolean containsX, double y, boolean containsY) {
        this.x = x;
        this.containsX = containsX;
        this.y = y;
        this.containsY = containsY;
    }

    //does not worry about the endpoints of this interval,
    // which is ok since the interval we apply it for is one from a segment so contains the endpoints always
    public boolean isDisjunctFast(Interval interval){
        if(interval.getY() < x || (interval.getY() == x && !interval.isContainsY())) return true;
        if(y < interval.getX() || (interval.getX() == y && !interval.isContainsX())) return true;
        return false;
    }

    //returns true if both intervals are disjunct
    public boolean isDisjunct(Interval interval){
        if(x <= interval.getX()){
            if(y < interval.getX()){
                return true;
            } else if(y == interval.getX() && (!containsY || !interval.isContainsX())){
                return true;
            }
        } else{
            if(interval.getY() < x){
                return true;
            } else if(interval.getY() == x && (!containsX || !interval.isContainsY())){
                return true;
            }
        }

        return false;
    }

    //does not worry about the endpoints of this interval,
    // which is ok since the interval we apply it for is one from a segment so contains the endpoints always
    public boolean containsFast(Interval interval){
        return x <= interval.getX() && y >= interval.getY();
    }

    //returns true if this contains the given interval
    public boolean contains(Interval interval){
        boolean xOk;
        boolean yOk;
        xOk = containsX ? (x <= interval.getX()) : (interval.isContainsX() ? x < interval.getX() : x <= interval.getX());
        yOk = containsY ? (interval.getY() <= y) : (interval.isContainsY() ? interval.getY() < y : interval.getY() <= y);
        return xOk && yOk;
    }

    public boolean contains(double d){
        if((x < d && d < y) || (d == x && containsX) || (d == y && containsY)) return true;
        return false;
    }

    public Interval split(double split, boolean left){
        if(left){
            return new Interval(x, containsX, split, true);
        } else{
            return new Interval(split, false, y, containsY);
        }
    }

    public double getX() {
        return x;
    }

    public boolean isContainsX() {
        return containsX;
    }

    public double getY() {
        return y;
    }

    public boolean isContainsY() {
        return containsY;
    }

    public boolean isOpenInterval(){
        return !containsX && !containsY;
    }

    public boolean isClosedInterval(){
        return containsX && containsY;
    }
}
