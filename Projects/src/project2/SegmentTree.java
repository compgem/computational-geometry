package project2;

import project2.segmentTree.STNode;

import java.util.*;

/**
 * Created by Karen on 25/11/2016.
 */
public class SegmentTree implements DataStructure{
    private STNode root;
    private Comparator<Segment> segmentComparator;

    public SegmentTree(List<Segment> segments){
        segmentComparator = new Comparator<Segment>() {
            @Override
            public int compare(Segment o1, Segment o2) {
                return Double.compare(o1.getY(), o2.getY());
            }
        };

        long startTime = System.nanoTime();
        double seconds;

        Set<Double> set = new HashSet<>();
        for(Segment s : segments){
            set.add(s.getX1());
            set.add(s.getX2());
        }
        List<Double> endpoints = new ArrayList<>(set);
        Collections.sort(endpoints);

        //long end1 = System.nanoTime();
        //seconds = (double)(end1-startTime)/1000000000.0;
        //System.out.println("Time for sorting all endpoints : " + seconds + " sec");

        //construct the elementary intervals
        List<Interval> intervals = new ArrayList<>();
        intervals.add(new Interval(-Double.MAX_VALUE, false, endpoints.get(0), false));
        for(int i = 0; i < endpoints.size() -1; i++){
            intervals.add(new Interval(endpoints.get(i), true, endpoints.get(i), true));
            intervals.add(new Interval(endpoints.get(i), false, endpoints.get(i+1), false));
        }
        intervals.add(new Interval(endpoints.get(endpoints.size()-1), true, endpoints.get(endpoints.size()-1), true));
        intervals.add(new Interval(endpoints.get(endpoints.size()-1), false, Double.MAX_VALUE, false));

        //long end2 = System.nanoTime();
        //seconds = (double)(end2-end1)/1000000000.0;
        //System.out.println("Time for adding intervals : " + seconds + " sec");

        //build the tree
        root = buildTree(intervals, 0, intervals.size() -1);

        //long end3 = System.nanoTime();
        //seconds = (double)(end3-end2)/1000000000.0;
        //System.out.println("Time for building tree : " + seconds + " sec");

        //add the intervals
        for(Segment segment : segments){
            insertSegmentTree(root, segment, new Interval(segment.getX1(), true, segment.getX2(), true));
        }

        //long end4 = System.nanoTime();
        //seconds = (double)(end4-end3)/1000000000.0;
        //System.out.println("Time for adding segments : " + seconds + " sec");

        //sort the intervals
        sortSegmentsInNodes(root);

        //long end5 = System.nanoTime();
        //seconds = (double)(end5-end4)/1000000000.0;
        //System.out.println("Time for sorting segments : " + seconds + " sec");
    }

    private void sortSegmentsInNodes(STNode node){
        Collections.sort(node.getSegments(), segmentComparator);
        if(node.getLeft() != null) sortSegmentsInNodes(node.getLeft());
        if(node.getRight() != null) sortSegmentsInNodes(node.getRight());
    }

    private void insertSegmentTree(STNode node, Segment segment, Interval interval){
        if(interval.containsFast(node.getInterval())){
            node.getSegments().add(segment);
        } else{
            if(!interval.isDisjunctFast(node.getLeft().getInterval())){
                insertSegmentTree(node.getLeft(), segment, interval);
            }
            if(!interval.isDisjunctFast(node.getRight().getInterval())){
                insertSegmentTree(node.getRight(), segment, interval);
            }
        }
    }

    @Override
    public List<Segment> query(Segment segment) {
        ArrayList<Segment> result = new ArrayList<>();
        queryST(segment, root, result);
        return result;
    }

    @Override
    public String getName() {
        return "segment tree";
    }
    
    @Override
    public int getSize(){
        int size = 0;
        
        if(root != null){
            size = getNodeSize(root);
        }           
        
        return size;
    }
    
    private int getNodeSize(STNode node){
        int size = 1;               
       
        if(node.getLeft() != null){
            size += getNodeSize(node.getLeft());
        }
        if(node.getRight() != null){
            size += getNodeSize(node.getRight());
        }        
        
        return size;
    }

    public int binarySearch(List<Segment> segments, double d){
        int low = 0;
        int high = segments.size()-1;
        while(high > low){
            int middle = (low + high) / 2;
            if(segments.get(middle).getY() == d) return middle;
            if(segments.get(middle).getY() < d){
                low = middle + 1;
            }
            if(segments.get(middle).getY() > d){
                high = middle -1;
            }
        }
        if(high == -1 || segments.get(high).getY() < d) return high + 1;
        return high;
    }

    public void queryST(Segment segment, STNode node, List<Segment> result){
        //output all the correct segments in this node
        if(!node.getSegments().isEmpty()){
            int m = binarySearch(node.getSegments(), segment.getX1());
            while(m < node.getSegments().size() && node.getSegments().get(m).getY() <= segment.getX2()){
                result.add(node.getSegments().get(m));
                m++;
            }
        }

        //recurse
        if(!node.isLeaf()){
            if(node.getLeft().getInterval().contains(segment.getY())){
                queryST(segment, node.getLeft(), result);
            } else{
                queryST(segment, node.getRight(), result);
            }
        }
    }

    public STNode buildTree(List<Interval> intervals, int start, int end){
        if(start <= end){
            STNode node = new STNode(new Interval(intervals.get(start).getX(), intervals.get(start).isContainsX(),
                    intervals.get(end).getY(), intervals.get(end).isContainsY()));

            int m = (end - start)/2;
            if(start != end){
                node.setLeft(buildTree(intervals, start, start + m));
                if(start < end){
                    node.setRight(buildTree(intervals, start + m + 1, end));
                }
            }
            return node;
        }
        return null;
    }

    public boolean inBetween(double x, double a, double b){
        return (a <= x) && (x <= b);
    }
}
