package project2;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Karen on 13/12/2016.
 */
public class InputGenerator {

    public List<Segment> generateInput1(double n, int size){
        Random r = new Random();
        double rangeMin = 1;
        double rangeMax = n;

        List<Segment> segments = new ArrayList<>();

        for(int i = 0; i < size; i++){
            double x = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            double y = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            double length = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            segments.add(new Segment(y, x, x+length, true));
        }
        return segments;
    }

    public List<Segment> generateInput2(double n, int size, double d){
        Random r = new Random();
        double rangeMin = 1;
        double rangeMax = n;

        List<Segment> segments = new ArrayList<>();

        for(int i = 0; i < size; i++){
            double x = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            double y = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
            segments.add(new Segment(y, x, x+d, true));
        }
        return segments;
    }

    public Segment getQuerySegment1(double n){
        Random r = new Random();
        double rangeMin = 1;
        double rangeMax = n;
        double x = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        double y = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        double length = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        return new Segment(y, x, x+length, false);
    }

    public Segment getQuerySegment2(double n, double length){
        Random r = new Random();
        double rangeMin = 1;
        double rangeMax = n;
        double x = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        double y = rangeMin + (rangeMax - rangeMin) * r.nextDouble();
        return new Segment(y, x, x+length, false);
    }
}
