package project2.kdtree;

import project2.Interval;
import project2.KDTree;
import project2.Point3D;

/**
 * Created by Karen on 11/12/2016.
 */
public class KDTNode {
    private Point3D point;
    private double splitpoint;
    private Interval x;
    private Interval y;
    private Interval z;
    private KDTNode left;
    private KDTNode right;
    private int level;

    public KDTNode(Point3D point, Interval x, Interval y, Interval z, int level) {
        this.point = point;
        this.x = x;
        this.y = y;
        this.z = z;
        this.level = level;
    }

    public double getSplitpoint() {
        return splitpoint;
    }

    public Point3D getPoint() {
        return point;
    }

    public void setSplitpoint(double splitpoint) {
        this.splitpoint = splitpoint;
    }

    public void setLeft(KDTNode left) {
        this.left = left;
    }

    public void setRight(KDTNode right) {
        this.right = right;
    }

    public Interval getX() {
        return x;
    }

    public Interval getY() {
        return y;
    }

    public Interval getZ() {
        return z;
    }

    public KDTNode getLeft() {
        return left;
    }

    public KDTNode getRight() {
        return right;
    }

    public int getLevel() {
        return level;
    }

    public boolean isLeaf(){
        return left == null && right == null;
    }
}
