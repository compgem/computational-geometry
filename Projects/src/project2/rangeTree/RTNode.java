package project2.rangeTree;

import project2.Point3D;

/**
 * Created by Karen on 20/12/2016.
 */
public class RTNode {
    private RTNode left;
    private RTNode right;
    //associated structure
    private RTNode asstree;
    Point3D split;
    Point3D point;


    public Point3D getPoint() {
        return point;
    }

    public void setPoint(Point3D point) {
        this.point = point;
    }

    public RTNode getLeft() {
        return left;
    }

    public void setLeft(RTNode left) {
        this.left = left;
    }

    public RTNode getRight() {
        return right;
    }

    public void setRight(RTNode right) {
        this.right = right;
    }

    public RTNode getAsstree() {
        return asstree;
    }

    public void setAsstree(RTNode asstree) {
        this.asstree = asstree;
    }

    public Point3D getSplit() {
        return split;
    }

    public void setSplit(Point3D split) {
        this.split = split;
    }

    public boolean isLeaf(){
        return left == null && right == null;
    }
}
