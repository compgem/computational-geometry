package project2;


import project1.Point;

import javax.swing.*;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Karen on 25/11/2016.
 */
public class Test {    
    public static void main(String[] args) throws IOException {
        System.out.println("====Testing for Input Size====");
        System.out.println("");
        testInputSize();
        System.out.println("====Testing for Output Size====");
        System.out.println("");
        testOutputSize();        
        //randomTest(500, 4750000);
    }
    
    //Tests for segment, interval and kd tree
    public static void testInputSize(){
        int n = 500;
        int iterations = 10;
        int[] inputSizes = {100, 1000, 5000, 10000, 25000, 50000, 75000, 100000, 150000, 250000, 500000, 750000, 1000000, 2000000, 3000000};
        InputGenerator ig = new InputGenerator();
        
        List<List<Segment>> inputs = new ArrayList<>();
        
        //generate inputs
        for(int inputSize: inputSizes){
            inputs.add(ig.generateInput1(n, inputSize));
        }      
        
        Segment query = ig.getQuerySegment1(n); 
        DataStructure ds;
        
        
        //Segment tree
        System.out.println("Segment tree:");
        System.out.println("n & Output size & Space & Query");
        for(int i = 0; i < 10; i++){
            System.out.print(inputSizes[i]);
            ds = new SegmentTree(inputs.get(i));
            int totalNumber = numberOfIntersections(inputs.get(i), query);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0; 
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(query);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            ds = null;
            System.gc();
            System.out.println("");
        }
        
        //Interval tree        
        System.out.println("Interval tree:");
        System.out.println("n & Output size & Space & Query");
        for(int i = 0; i < inputs.size(); i++){
            System.out.print(inputSizes[i]);
            ds = new IntervalTree(inputs.get(i));
            int totalNumber = numberOfIntersections(inputs.get(i), query);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(query);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            ds = null;
            System.gc();
            System.out.println("");
        }
        
        //KD-tree
        System.out.println("KD-tree:");
        System.out.println("n & Output size & Space & Query");
        for(int i = 0; i < 13; i++){
            System.out.print(inputSizes[i]);
            ds = new KDTree(inputs.get(i));
            int totalNumber = numberOfIntersections(inputs.get(i), query);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(query);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            ds = null;
            System.gc();
            System.out.println("");
        }
        
        //Range tree
        inputs.removeAll(inputs.subList(7, inputs.size()));
        System.out.println("Range tree:");
        System.out.println("n & Output size & Space & Query");
        for(int i = 0; i < 7; i++){
            System.out.print(inputSizes[i]);
            ds = new RangeTree(inputs.get(i));
            int totalNumber = numberOfIntersections(inputs.get(i), query);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(query);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            inputs.set(i, null);
            ds = null;
            System.gc();
            System.out.println("");
        }       
    }
    
    
    public static void testOutputSize(){
        int n = 500000;
        int iterations = 10;
        int[] inputSizes = {50000, 500000};
        int[] distances = {1000, 10000, 50000, 100000, 500000, 1000000, 5000000, 10000000, 100000000}; //{10, 50, 100, 200, 300, 400, 500, 1000, 2500, 5000, 7500, 10000, 20000, 50000, 100000, 500000};
        InputGenerator ig = new InputGenerator();
        
        List<List<Segment>> inputsSmall = new ArrayList<>();
        List<List<Segment>> inputsLarge = new ArrayList<>();
        Segment queries[] = new Segment[distances.length];
        
        //generate inputs
        for(int d: distances){
            inputsSmall.add(ig.generateInput2(n, inputSizes[0], d));
            inputsLarge.add(ig.generateInput2(n, inputSizes[1], d));
        }  
        
        //generate queries
        for(int d = 0; d < distances.length; d++){
            Segment query = ig.getQuerySegment2(n, distances[d]);
            queries[d] = query;
        }        
         
        DataStructure ds;        
        
        //Segment tree
        System.out.println("Segment tree:");
        System.out.println("d & Output size & Space & Query");
        for(int i = 0; i < distances.length; i++){
            System.out.print(distances[i]);
            ds = new SegmentTree(inputsLarge.get(i));
            int totalNumber = numberOfIntersections(inputsLarge.get(i), queries[i]);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(queries[i]);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            ds = null;
            System.gc();
            System.out.println("");
        }
        
        //Interval tree        
        System.out.println("Interval tree:");
        System.out.println("d & Output size & Space & Query");
        for(int i = 0; i < distances.length; i++){
            System.out.print(distances[i]);
            ds = new IntervalTree(inputsLarge.get(i));
            int totalNumber = numberOfIntersections(inputsLarge.get(i), queries[i]);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(queries[i]);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            ds = null;
            System.gc();
            System.out.println("");
        }
        
        //KD-tree
        System.out.println("KD-tree:");
        System.out.println("d & Output size & Space & Query");
        for(int i = 0; i < distances.length; i++){
            System.out.print(distances[i]);
            ds = new KDTree(inputsLarge.get(i));
            int totalNumber = numberOfIntersections(inputsLarge.get(i), queries[i]);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(queries[i]);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            ds = null;
            System.gc();
            System.out.println("");
        }
        
        //Range tree
        System.out.println("Range tree:");
        System.out.println("d & Output size & Space & Query");
        for(int i = 0; i < distances.length; i++){
            System.out.print(distances[i]);
            ds = new RangeTree(inputsSmall.get(i));
            int totalNumber = numberOfIntersections(inputsSmall.get(i), queries[i]);
            System.out.print(" & " + totalNumber);
            int size = ds.getSize();
            System.out.print(" & " + size);
            double result = 0;
            List<Segment> output = null;
            for(int it = 0; it < iterations; it++){
                long startTime = System.nanoTime();
                output = ds.query(queries[i]);
                long endTime = System.nanoTime();
                result += (endTime-startTime);
            }
            result = result/ ((double) iterations);
            double seconds = result / 1000000000.0;
            System.out.print(" & " + seconds);
            
            //inputs.set(i, null);
            ds = null;
            System.gc();
            System.out.println("");
        }
    }

    public static void miniTest(){
        System.out.println("Start mini test...");
        List<Segment> input = new ArrayList<>();
        input.add(new Segment(4,1,3, true));
        input.add(new Segment(3,2,4, true));
        input.add(new Segment(2,1,2, true));
        input.add(new Segment(2,3,4, true));
        input.add(new Segment(1,1,4, true));

        DataStructure ds = new IntervalTree(input);

        Segment q1 = new Segment(1.5,0.5,3, false);
        List<Segment> output1 = ds.query(q1);
        if(output1.size() != 2 || !checkResult(output1, q1)) System.out.println("Fout in query 1.");

        Segment q2 = new Segment(2.5,0,5, false);
        List<Segment> output2 = ds.query(q2);
        if(output2.size() != 3 || !checkResult(output2, q2)) System.out.println("Fout in query 2.");

    }

    public static void randomTest(double n, int size){
        InputGenerator ig = new InputGenerator();
        List<Segment> input = ig.generateInput1(n, size);
        long start, end;

        int dsSize = 1;
        DataStructure ds[] = new DataStructure[dsSize];

        /*long start = System.nanoTime();
        DataStructure ds[] = new DataStructure[]{new IntervalTree(input)};
        long end = System.nanoTime();
        double sec = (double)(end-start)/1000000000.0;
        System.out.println("Time taken to build tree: " + sec + " sec");
        int dsSize = 1;*/
        
        double sec;
        
        /*start = System.nanoTime();
        ds[0] = new SegmentTree(input);
        end = System.nanoTime();
        sec = (double)(end-start)/1000000000.0;
        System.out.println("Time taken to build segment tree: " + sec + " sec");
        System.out.println("Size of segment tree: " + ds[0].getSize());
        System.out.println("");*/

        start = System.nanoTime();
        ds[0] = new IntervalTree(input);
        end = System.nanoTime();
        sec = (double)(end-start)/1000000000.0;
        System.out.println("Time taken to build interval tree: " + sec + " sec");
        System.out.println("Size of interval tree: " + ds[0].getSize());
        System.out.println("");

        /*start = System.nanoTime();
        ds[0] = new KDTree(input);
        end = System.nanoTime();
        sec = (double)(end-start)/1000000000.0;
        System.out.println("Time taken to build KD tree: " + sec + " sec");
        System.out.println("Size of KD tree: " + ds[0].getSize());
        System.out.println("");*/

        /*start = System.nanoTime();
        ds[0] = new RangeTree(input);
        end = System.nanoTime();
        sec = (double)(end-start)/1000000000.0;
        System.out.println("Time taken to build range tree: " + sec + " sec");
        System.out.println("Size of range tree: " + ds[0].getSize());*/



        Segment query = ig.getQuerySegment2(n, n);

        int totalNumber = numberOfIntersections(input, query);
        System.out.println("Output size : " + totalNumber);

        for(int i = 0; i < dsSize; i++){
            System.out.println("Starting test for datastructure: " + ds[i].getName());
            long startTime = System.nanoTime();
            List<Segment> output = ds[i].query(query);
            long endTime = System.nanoTime();
            double seconds = (double)(endTime-startTime)/1000000000.0;
            System.out.println("Time taken to query: " + seconds + " sec");
            if(output.size() != totalNumber || !checkResult(output, query)) System.out.println("But something went wrong..");
            System.out.println("");

            //drawOutput(input, output, query);
        }

    }

    public static int numberOfIntersections(List<Segment> segments, Segment query){
        int size = 0;
        for(int i = 0; i < segments.size(); i++){
            double sx1 = segments.get(i).getX1();
            double sx2 = segments.get(i).getX2();
            double sy = segments.get(i).getY();
            if(inBetween(query.getY(), sx1, sx2) && inBetween(sy, query.getX1(), query.getX2())) size++;

        }
        return size;
    }

    public static void testBinarySearch(){
        List<Segment> segments = new ArrayList<>();
        segments.add(new Segment(1, 1, 1, true));
        segments.add(new Segment(2, 1, 1, true));
        segments.add(new Segment(4, 1, 1, true));
        segments.add(new Segment(6, 1, 1, true));
        segments.add(new Segment(8, 1, 1, true));
        segments.add(new Segment(10, 1, 1, true));
        segments.add(new Segment(12, 1, 1, true));
        segments.add(new Segment(14, 1, 1, true));

        SegmentTree st = new SegmentTree(segments);
        if(st.binarySearch(segments, 3) != 2) System.out.println("Fout! 1");
        if(st.binarySearch(segments, 8) != 4) System.out.println("Fout! 2");
        if(st.binarySearch(segments, 15) != 8) System.out.println("Fout! hihi 3");
        if(st.binarySearch(segments, 2) != 1) System.out.println("Fout! 4");
        if(st.binarySearch(segments, 11) != 6) System.out.println("Fout! 5");
        if(st.binarySearch(segments, 5) != 3) System.out.println("Fout! 6");
    }

    public static void test1(){
        System.out.println("Start test1...");
        List<Segment> input = new ArrayList<>();
        input.add(new Segment(7,3.5,14.5, true));
        input.add(new Segment(6,1,3, true));
        input.add(new Segment(6,7,11, true));
        input.add(new Segment(5,2.5,6, true));
        input.add(new Segment(5,9,13.5, true));
        input.add(new Segment(4,6.5,10.5, true));
        input.add(new Segment(4,13,16.5, true));
        input.add(new Segment(3,3.5,7, true));
        input.add(new Segment(3,9,13, true));
        input.add(new Segment(2,1,5.5, true));
        input.add(new Segment(2,11.5,12.5, true));
        input.add(new Segment(2,13.5,14.5, true));
        input.add(new Segment(2,15.5,16.5, true));
        input.add(new Segment(1,6,10, true));
        input.add(new Segment(1,12.5,15.5, true));

        DataStructure ds = new IntervalTree(input);

        Segment q1 = new Segment(2,2.5,5.5, false);
        List<Segment> output1 = ds.query(q1);
        if(output1.size() != 0 || !checkResult(output1, q1)) System.out.println("Fout in query 1.");

        Segment q2 = new Segment(8,0.5,8, false);
        List<Segment> output2 = ds.query(q2);
        if(output2.size() != 4 || !checkResult(output2, q2)) System.out.println("Fout in query 2.");

        Segment q3 = new Segment(9,3,5.5, false);
        List<Segment> output3 = ds.query(q3);
        if(output3.size() != 3 || !checkResult(output3, q3)) System.out.println("Fout in query 3.");

        Segment q4 = new Segment(13.5,1.5,7.5, false);
        List<Segment> output4 = ds.query(q4);
        if(output4.size() != 4 || !checkResult(output4, q4)) System.out.println("Fout in query 4.");

        Segment q5 = new Segment(9.5,0,17, false);
        List<Segment> output5 = ds.query(q5);
        if(output5.size() != 6 || !checkResult(output5, q5)) System.out.println("Fout in query 5.");

        drawOutput(input, output5, q5);
        System.out.println("Tests completed.");
    }

    public static void testContainsInterval(){
        Interval i1 = new Interval(2, true, 6, true);
        Interval i2 = new Interval(2, false, 6, false);
        Interval i3 = new Interval(3, true, 6, true);

        if(!i1.contains(i2)) System.out.println("Fout!");
        if(i2.contains(i1)) System.out.println("Fout!");
        if(!i1.contains(i3)) System.out.println("Fout!");
        if(i2.contains(i3)) System.out.println("Fout!");
        if(i3.contains(i1)) System.out.println("Fout!");
    }

    public static void testIsDisjunct(){
        Interval i1 = new Interval(1, true, 3, true);
        Interval i2 = new Interval(1, true, 3, false);
        Interval i3 = new Interval(3, true, 3, true);

        if(i1.isDisjunct(i3)) System.out.println("Fout!");
        if(!i2.isDisjunct(i3)) System.out.println("Fout!");
        if(i1.isDisjunct(i2)) System.out.println("Fout!");

    }

    public static void testBuildTree(){
        List<Segment> segments = new ArrayList<>();
        segments.add(new Segment(1, 1,3,true));
        segments.add(new Segment(2, 2,4,true));
        segments.add(new Segment(3, 5,7,true));
        segments.add(new Segment(4, 6,7,true));
        segments.add(new Segment(5, 2,6,true));

        SegmentTree st = new SegmentTree(segments);

        drawOutput(segments, new ArrayList<>(), new Segment(1,1,1,false));
    }

    public static boolean checkResult(List<Segment> result, Segment query){
        for(int i = 0; i < result.size(); i ++){
            if(!inBetween(query.getY(), result.get(i).getX1(), result.get(i).getX2()) ||
                    !inBetween(result.get(i).getY(), query.getX1(), query.getX2())) return false;
        }
        return true;
    }

    //returns true if x lies in between (or on) a and b
    public static boolean inBetween(double x, double a, double b){
        return (a <= x) && (x <= b);
    }

    public static void drawOutput(List<Segment> input, List<Segment> result, Segment query){
        JFrame frame = new JFrame("FrameDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setSize(1000,600);

        frame.add(new Drawing(input, result, query));

        frame.setVisible(true);
    }
}
