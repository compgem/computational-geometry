package project2;

/**
 * Created by Karen on 20/12/2016.
 */
public class UniquePoint {
    double p1;
    double p2;
    double p3;

    public UniquePoint(double p1, double p2, double p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    public double getP1() {
        return p1;
    }

    public double getP2() {
        return p2;
    }

    public double getP3() {
        return p3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UniquePoint that = (UniquePoint) o;

        if (Double.compare(that.p1, p1) != 0) return false;
        if (Double.compare(that.p2, p2) != 0) return false;
        return Double.compare(that.p3, p3) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(p1);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(p2);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(p3);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public int compare(UniquePoint up){
        int i = Double.compare(p1, up.getP1());
        if(i != 0) return i;

        i = Double.compare(p2, up.getP2());
        if(i != 0) return i;

        return Double.compare(p3, up.getP3());
    }
}
