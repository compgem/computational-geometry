package project2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import project2.intervalTree.ITNode;
import project2.prioritySearchTree.PSTNode;

/**
 * Created by Karen on 25/11/2016.
 */
public class IntervalTree implements DataStructure {
    private ITNode root;
    
    public IntervalTree(List<Segment> segments){

        root = buildTree(segments);
        
        //construct the Priority Search Trees
        constructPSTsInNodes(root);        
    }

    @Override
    public List<Segment> query(Segment segment) {
        List<Segment> result = new ArrayList<>();
        
        queryIT(result, segment, root);
        
        return result;
    }

    public int checkTree() {
        return checkTree(root);
    }

    //returns the size of the total amount of segments stored in the tree
    public int checkTree(ITNode node){
        int i = node.getSegments().size();
        if(node.getLeftPST().size() != i){
            System.out.println("================= WRONG SIZE LEFT, is " +node.getLeftPST().size() + " should be " + i);
        }
        if(node.getRightPST().size() != i) System.out.println("================ WRONG SIZE RIGHT, is "+node.getRightPST().size() + " should be " + i);


        if(node.getLeft() != null){
            i += checkTree(node.getLeft());
        }
        if(node.getRight() != null){
            i += checkTree(node.getRight());
        }
        return i;
    }

    @Override
    public String getName() {
        return "interval tree";
    }
    
    @Override
    public int getSize(){
        int size = 0;
        
        if(root != null){
            size = getNodeSize(root);
        }           
        
        return size;
    }
    
    private int getNodeSize(ITNode node){
        int size = (1 + node.getLeftPST().size() + node.getRightPST().size());      
               
        if(node.getLeft() != null){
            size += getNodeSize(node.getLeft());
        }
        if(node.getRight() != null){
            size += getNodeSize(node.getRight());
        }        
        
        return size;
    }
    
    public ITNode buildTree(List<Segment> segments) {
        if(segments.size() == 1){
            ITNode node = new ITNode();
            node.insertSegment(segments.get(0));
            return node;
        }
        if(!segments.isEmpty()) {
            List<Double> endpoints = new ArrayList<>();
            for(Segment s : segments){
                endpoints.add(s.getX2());
                endpoints.add(s.getX1());
            }

            Collections.sort(endpoints);
            double XMid = endpoints.get(endpoints.size()/2);

            ITNode node = new ITNode();
            node.setMid(XMid);

            List<Segment> IMid = new ArrayList<>();       
            List<Segment> ILeft = new ArrayList<>();
            List<Segment> IRight = new ArrayList<>();

            for(Segment s : segments){
                if(s.getX1() <= XMid && XMid <= s.getX2()){
                    IMid.add(s);
                } else if(s.getX2() < XMid){
                    ILeft.add(s);
                } else if(s.getX1() > XMid){
                    IRight.add(s);
                }
            }

            for(Segment s : IMid){
                node.insertSegment(s);
            }
            
            node.setLeft(buildTree(ILeft));           
            node.setRight(buildTree(IRight));                            

            return node;
        }
        return new ITNode();
    }
    
    public void constructPSTsInNodes(ITNode node){
        node.constructPSTs();
        
        if(node.getLeft() != null){
            constructPSTsInNodes(node.getLeft());
        }
        if(node.getRight() != null){
            constructPSTsInNodes(node.getRight());
        }
    }
    
    public double computeMedianEndpoints(List<Segment> segments) {
        List<Double> endpoints = new ArrayList<>();
        
        for(Segment s: segments){
            endpoints.add(s.getX1());
            endpoints.add(s.getX2());
        } 
        
        Collections.sort(endpoints);
        double median;
        if (endpoints.size()%2 == 0){
            median = (endpoints.get(endpoints.size()/2) + endpoints.get(endpoints.size()/2 - 1))/2;            
        }            
        else{
            median = endpoints.get((endpoints.size()-1)/2);
        }
        
        return median;            
    }

    public List<Segment> queryIT(List<Segment> result, Segment segment, ITNode node) {
        if(node.isLeaf()){
            if(!node.getSegments().isEmpty()){
                Segment s = node.getSegments().get(0);

                boolean xOK = segment.getX1() <= s.getY() && s.getY() <= segment.getX2();
                boolean yOk = s.getX1() <= segment.getY() && segment.getY() <= s.getX2();
                if(xOK && yOk) result.add(s);
            }
            return result;
        }

        double x = segment.getY();
        double y1 = segment.getX1();
        double y2 = segment.getX2();
        if(x <= node.getMid()){
            List<Point> resultPSTLeft = node.getLeftPST().query(x, y1, y2, true);
            for(Point p : resultPSTLeft){
                result.add(p.getOwner());
            }
            queryIT(result, segment, node.getLeft());
        } else{
            List<Point> resultPSTRight = node.getRightPST().query(x, y1, y2, false);
            for(Point p : resultPSTRight){
                result.add(p.getOwner());
            }
            queryIT(result, segment, node.getRight());
        }

        return result;
    }

}
