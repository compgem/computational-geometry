package project2;

import project1.Point;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Line2D;
import java.util.List;

/**
 * Created by Karen on 4/12/2016.
 */

public class Drawing extends JPanel {

    private List<Segment> input;
    private List<Segment> result;
    private Segment query;
    private double scale;

    public Drawing(List<Segment> input, List<Segment> result, Segment query){
        this.input = input;
        this.result = result;
        this.query = query;
        scale = 1;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        Graphics2D g2d = (Graphics2D) g;

        g2d.scale(1, -1);
        g2d.translate(0, -getHeight());

        //draw the input
        g2d.setColor(Color.blue);

        for (int i = 0; i < input.size(); i++) {
            Shape p = new Line2D.Double(input.get(i).getX1()*scale, input.get(i).getY()*scale, input.get(i).getX2()*scale, input.get(i).getY()*scale);
            g2d.draw(p);
        }

        //draw the result
        g2d.setColor(Color.RED);
        for (int i = 0; i < result.size(); i++) {
            Shape p = new Line2D.Double(result.get(i).getX1()*scale, result.get(i).getY()*scale, result.get(i).getX2()*scale, result.get(i).getY()*scale);
            g2d.draw(p);
        }

        //draw the query segment
        g2d.setColor(Color.black);
        g2d.setStroke(new BasicStroke(2));

        Shape p = new Line2D.Double(query.getY()*scale, query.getX1()*scale, query.getY()*scale, query.getX2()*scale);
        g2d.draw(p);
    }

}
