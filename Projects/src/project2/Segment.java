package project2;

/**
 * Created by Karen on 25/11/2016.
 */
public class Segment {
    private boolean horizontal;
    private double y;
    private double x1;
    private double x2;

    public Segment(double y, double x1, double x2, boolean horizontal){
        this.x1 = x1;
        this.x2 = x2;
        this.y = y;
        this.horizontal = horizontal;
    }

    public double getY() {
        return y;
    }

    public double getX1() {
        return x1;
    }

    public double getX2() {
        return x2;
    }

    public boolean isHorizontal() {
        return horizontal;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Segment segment = (Segment) o;

        if (horizontal != segment.horizontal) return false;
        if (Double.compare(segment.y, y) != 0) return false;
        if (Double.compare(segment.x1, x1) != 0) return false;
        return Double.compare(segment.x2, x2) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (horizontal ? 1 : 0);
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(x1);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(x2);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
