package project2;

/**
 *
 * @author jeroen
 */
public class Point {

    private final double x;
    private final double y;
    private Segment owner;

    //Create a point with owner
    public Point(double xCoord, double yCoord, Segment owner) {
        this.x = xCoord;
        this.y = yCoord;
        this.owner = owner;
    }

    //Create a point without label
    public Point(double xCoord, double yCoord) {
        this.x = xCoord;
        this.y = yCoord;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
    
    public Segment getOwner(){
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Point point = (Point) o;

        if (Double.compare(point.x, x) != 0) return false;
        if (Double.compare(point.y, y) != 0) return false;
        return owner != null ? owner.equals(point.owner) : point.owner == null;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(x);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(y);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        return result;
    }
}
