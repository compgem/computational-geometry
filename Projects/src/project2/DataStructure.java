package project2;

import java.util.List;

/**
 * Created by Karen on 25/11/2016.
 */
public interface DataStructure {
    public List<Segment> query(Segment segment);

    public String getName();
    
    public int getSize();
}
