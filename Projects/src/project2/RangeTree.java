package project2;

import project1.*;
import project2.kdtree.KDTNode;
import project2.rangeTree.RTNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Karen on 20/12/2016.
 */
public class RangeTree implements DataStructure {
    private RTNode root;
    private PointsHandler handler;

    public RangeTree(List<Segment> segments){
        handler = new PointsHandler();
        List<Point3D> points = handler.transformSegmentToPoint(segments);
        handler.sort(points, 0);
        root = buildTree(points, 0);
    }

    @Override
    public List<Segment> query(Segment segment) {
        UniqueInterval x = new UniqueInterval(-Double.MAX_VALUE, false, segment.getY(), true, 0);
        UniqueInterval y = new UniqueInterval(segment.getY(), true, Double.MAX_VALUE, false, 1);
        UniqueInterval z = new UniqueInterval(segment.getX1(), true, segment.getX2(), true, 2);
        List<Point3D> result = new ArrayList<>();
        RTquery3D(root, result, x, y, z);
        return handler.transformPointToSegment(result);
    }

    public RTNode findSplitNode(RTNode node, Point3D x, Point3D y, int level){
        RTNode v = node;
        while(!v.isLeaf() && (PointsHandler.compare(y,v.getSplit(),level) <= 0 ||
                PointsHandler.compare(x,v.getSplit(),level) == 1)){
            if(PointsHandler.compare(y,v.getSplit(),level) <= 0){
                v = v.getLeft();
            } else{
                v = v.getRight();
            }
        }
        return v;
    }

    public void RTquery3D(RTNode node, List<Point3D> result, UniqueInterval x, UniqueInterval y, UniqueInterval z){
        RTNode split = findSplitNode(node, x.getX(), x.getY(), 0);
        if(split.isLeaf()){
            Point3D point = split.getPoint();
            if(x.contains(point.getX(),0) && y.contains(point.getY(),1) && z.contains(point.getZ(),2)) result.add(point);
        } else{
            RTNode vl = split.getLeft();
            while(vl != null && !vl.isLeaf()){
                if(handler.compare(x.getX(), vl.getSplit(),0) <= 0){
                    RTquery2D(vl.getRight().getAsstree(), result, y, z);
                    vl = vl.getLeft();
                } else{
                    vl = vl.getRight();
                }
            }
            if(vl != null){
                Point3D point = vl.getPoint();
                if(x.contains(point.getX(),0) && y.contains(point.getY(),1) && z.contains(point.getZ(),2)) result.add(point);
            }

            RTNode vr = split.getRight();
            while(vr != null && !vr.isLeaf()){
                if(handler.compare(x.getY(),vr.getSplit(),0) == 1){
                    RTquery2D(vr.getLeft().getAsstree(), result, y, z);
                    vr = vr.getRight();
                } else{
                    vr = vr.getLeft();
                }
            }
            if(vr != null){
                Point3D point = vr.getPoint();
                if(x.contains(point.getX(),0) && y.contains(point.getY(),1) && z.contains(point.getZ(),2)) result.add(point);
            }

        }
    }



    public void RTquery2D(RTNode node, List<Point3D> result, UniqueInterval y, UniqueInterval z) {
        RTNode split = findSplitNode(node, y.getX(), y.getY(), 1);
        if(split.isLeaf()){
            Point3D point = split.getPoint();
            if(y.contains(point.getY(),1) && z.contains(point.getZ(),2)) result.add(point);
        } else{
            RTNode vl = split.getLeft();
            while(vl != null && !vl.isLeaf()){
                if(handler.compare(y.getX(),vl.getSplit(),1) <= 0){
                    RTquery1D(vl.getRight().getAsstree(), result, z);
                    vl = vl.getLeft();
                } else{
                    vl = vl.getRight();
                }
            }
            if(vl != null){
                Point3D point = vl.getPoint();
                if(y.contains(point.getY(),1) && z.contains(point.getZ(),2)) result.add(point);
            }

            RTNode vr = split.getRight();
            while(vr != null && !vr.isLeaf()){
                if(handler.compare(y.getY(),vr.getSplit(),1) == 1){
                    RTquery1D(vr.getLeft().getAsstree(), result, z);
                    vr = vr.getRight();
                } else{
                    vr = vr.getLeft();
                }
            }
            if(vr != null){
                Point3D point = vr.getPoint();
                if(y.contains(point.getY(),1) && z.contains(point.getZ(),2)) result.add(point);
            }
        }
    }

    public void RTquery1D(RTNode node, List<Point3D> result, UniqueInterval z) {
        RTNode split = findSplitNode(node, z.getX(), z.getY(), 2);
        if(split.isLeaf()){
            Point3D point = split.getPoint();
            if(z.contains(point.getZ(),2)) result.add(point);
        } else{
            RTNode vl = split.getLeft();
            while(vl != null && !vl.isLeaf()){
                if(handler.compare(z.getX(),vl.getSplit(),2) <= 0){
                    reportSubtree(vl.getRight(), result);
                    vl = vl.getLeft();
                } else{
                    vl = vl.getRight();
                }
            }
            if(vl != null){
                Point3D point = vl.getPoint();
                if(z.contains(point.getZ(),2)) result.add(point);
            }

            RTNode vr = split.getRight();
            while(vr != null && !vr.isLeaf()){
                if(handler.compare(z.getY(),vr.getSplit(),2) == 1){
                    reportSubtree(vr.getLeft(), result);
                    vr = vr.getRight();
                } else{
                    vr = vr.getLeft();
                }
            }
            if(vr != null){
                Point3D point = vr.getPoint();
                if(z.contains(point.getZ(),2)) result.add(point);
            }
        }
    }



        @Override
    public String getName() {
        return "range tree";
    }
    
    @Override
    public int getSize(){
        int size = 0;
        
        if(root != null){
            size = getNodeSize(root);
        }           
        
        return size;
    }
    
    private int getNodeSize(RTNode node){
        int size = 1;               
       
        if(node.getLeft() != null){
            size += getNodeSize(node.getLeft());
        }
        if(node.getRight() != null){
            size += getNodeSize(node.getRight());
        }   
        if(node.getAsstree() != null){
            size += getNodeSize(node.getAsstree());
        }
        
        return size;
    }

    private RTNode buildTree(List<Point3D> points, int depth){
        RTNode node = new RTNode();
        if(depth < 2){
            List<Point3D> points2 = new ArrayList<>(points);
            handler.sort(points2, depth+1);
            node.setAsstree(buildTree(points2, depth+1));
        }

        if(points.size() == 1){
            node.setPoint(points.get(0));
            return node;
        } else{
            int m = (points.size()-1)/2;
            Point3D split = points.get(m);

            List<Point3D> p1 = points.subList(0, m+1);
            List<Point3D> p2 = points.subList(m+1, points.size());

            node.setSplit(split);

            node.setLeft(buildTree(p1, depth));
            node.setRight(buildTree(p2, depth));

            return node;
        }
    }

    public void reportSubtree(RTNode node, List<Point3D> points){
        if(node.isLeaf()){
            points.add(node.getPoint());
        } else{
            if(node.getLeft() != null) reportSubtree(node.getLeft(), points);
            if(node.getRight() != null) reportSubtree(node.getRight(), points);
        }
    }
}
