package project2;

import java.util.DoubleSummaryStatistics;

/**
 * Created by Karen on 20/12/2016.
 */
public class UniqueInterval {
    private Point3D x;
    private boolean containsX;
    private Point3D y;
    private boolean containsY;
    PointsHandler h;

    public UniqueInterval(double x, boolean containsX, double y, boolean containsY, int dimension){
        h = new PointsHandler();
        this.containsX = containsX;
        this.containsY = containsY;
        if(dimension == 0){
            this.x = new Point3D(x, -Double.MAX_VALUE, -Double.MAX_VALUE);
            this.y = new Point3D(y, Double.MAX_VALUE, Double.MAX_VALUE);
        }
        if(dimension == 1){
            this.x = new Point3D(-Double.MAX_VALUE, x, -Double.MAX_VALUE);
            this.y = new Point3D(Double.MAX_VALUE, y, Double.MAX_VALUE);
        }
        if(dimension == 2){
            this.x = new Point3D(-Double.MAX_VALUE, -Double.MAX_VALUE, x);
            this.y = new Point3D(Double.MAX_VALUE, Double.MAX_VALUE, y);
        }


    }


    public UniqueInterval(double x, boolean containsX, double y, boolean containsY) {
        this(x, containsX, y, containsY, 0);
    }

    //does not worry about the endpoints of this interval,
    // which is ok since the interval we apply it for is one from a segment so contains the endpoints always
    public boolean isDisjunctFast(UniqueInterval interval, int dimension){
        if(h.compare(interval.getY(),x,0) == -1 || ( h.compare(interval.getY(),x,0) == 0 && !interval.isContainsY())) return true;
        if(h.compare(y,interval.getX(),0) == -1 || ( h.compare(interval.getX(),y,0) == 0 && !interval.isContainsX())) return true;
        return false;
    }

    //returns true if both intervals are disjunct
    public boolean isDisjunct(UniqueInterval interval){
        if(h.compare(x,interval.getX(), 0) <= 0){
            if(h.compare(y,interval.getX(),0) == -1){
                return true;
            } else if(h.compare(y,interval.getX(),0)==0 && (!containsY || !interval.isContainsX())){
                return true;
            }
        } else{
            if(h.compare(interval.getY(),x,0) == -1){
                return true;
            } else if(h.compare(interval.getY(),x,0) == 0 && (!containsX || !interval.isContainsY())){
                return true;
            }
        }

        return false;
    }

    //does not worry about the endpoints of this interval,
    // which is ok since the interval we apply it for is one from a segment so contains the endpoints always
    public boolean containsFast(UniqueInterval interval){
        return h.compare(x,interval.getX(),0) <= 0 && h.compare(y,interval.getY(),0) >= 0;
    }

    //returns true if this contains the given interval
    public boolean contains(UniqueInterval interval){
        boolean xOk;
        boolean yOk;
        xOk = containsX ? (h.compare(x,interval.getX(),0) <= 0): (interval.isContainsX() ? h.compare(x,interval.getX(),0) ==-1 :
                h.compare(x,interval.getX(),0) <= 0);
        yOk = containsY ? (h.compare(interval.getY(),y,0) <=0) : (interval.isContainsY() ? h.compare(interval.getY(),y,0) ==-1 :
                h.compare(interval.getY(),y,0) <= 0);
        return xOk && yOk;
    }

    public boolean contains(double d, int dimension){
        if(dimension == 0){
            if((x.getX() < d && d < y.getX()) || (d == x.getX() && containsX) || (d == y.getX() && containsY)) return true;
        }
        if(dimension == 1){
            if((x.getY() < d && d < y.getY()) || (d == x.getY() && containsX) || (d == y.getY() && containsY)) return true;
        }
        if(dimension == 2){
            if((x.getZ() < d && d < y.getZ()) || (d == x.getZ() && containsX) || (d == y.getZ() && containsY)) return true;
        }
        return false;
    }

    public UniqueInterval split(double split, boolean left){
        if(left){
            return new UniqueInterval(x.getX(), containsX, split, true);
        } else{
            return new UniqueInterval(split, false, y.getX(), containsY);
        }
    }

    public Point3D getX() {
        return x;
    }

    public boolean isContainsX() {
        return containsX;
    }

    public Point3D getY() {
        return y;
    }

    public boolean isContainsY() {
        return containsY;
    }

    public boolean isOpenInterval(){
        return !containsX && !containsY;
    }

    public boolean isClosedInterval(){
        return containsX && containsY;
    }
}
