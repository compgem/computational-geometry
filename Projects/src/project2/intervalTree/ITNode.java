package project2.intervalTree;

import java.util.ArrayList;
import java.util.List;
import project2.Interval;
import project2.Point;
import project2.PrioritySearchTree;
import project2.Segment;
import project2.prioritySearchTree.PSTNode;

/**
 *
 * @author jeroen
 */
public class ITNode {
    private ITNode left;
    private ITNode right;
    private double XMid;
    private List<Segment> segments;
    //private Interval interval;
    private PrioritySearchTree PSTLeft;
    private PrioritySearchTree PSTRight;
       
    /*public ITNode(Interval interval) {
        this.left = null;
        this.right = null;
        this.interval = interval;
        segments = new ArrayList<>();
    }  */

    public ITNode() {
        this.left = null;
        this.right = null;
        segments = new ArrayList<>();
    }

    public List<Segment> getSegments() {
        return segments;
    }

    public boolean isLeaf(){
        return left == null && right == null;
    }

    /*public Interval getInterval() {
        return interval;
    }*/
    
    public ITNode getLeft() {
        return left;
    }

    public ITNode getRight() {
        return right;
    }

    public void setLeft(ITNode left) {
        this.left = left;
    }

    public void setRight(ITNode right) {
        this.right = right;
    }
    
    public void setMid(double median) {
        this.XMid = median; 
    }
    
    public double getMid() {
        return XMid;
    }
    
    public PrioritySearchTree getLeftPST(){
        return PSTLeft;
    }
    
    public PrioritySearchTree getRightPST(){
        return PSTRight;
    }
    
    public void insertSegment(Segment segment) {
        this.segments.add(segment);
    }
    
    public void constructPSTs() { 
        List<Point> leftEndPoints = new ArrayList<>();
        List<Point> rightEndPoints = new ArrayList<>();
        
        for(Segment s: segments){
            leftEndPoints.add(new Point(s.getX1(), s.getY(), s));
            rightEndPoints.add(new Point(s.getX2(), s.getY(), s));
        }
        
        this.PSTLeft = new PrioritySearchTree(leftEndPoints, true);
        this.PSTRight = new PrioritySearchTree(rightEndPoints, false);
    }
}
