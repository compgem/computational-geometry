package project2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Karen on 20/12/2016.
 */
public class PointsHandler {
    private Comparator<Point3D> compareX;
    private Comparator<Point3D> compareY;
    private Comparator<Point3D> compareZ;

    public PointsHandler(){
        compareX = new Comparator<Point3D>() {
            @Override
            public int compare(Point3D o1, Point3D o2) {
                return PointsHandler.compare(o1, o2, 0);
            }
        };
        compareY = new Comparator<Point3D>() {
            @Override
            public int compare(Point3D o1, Point3D o2) {
                return PointsHandler.compare(o1, o2, 1);
            }
        };
        compareZ = new Comparator<Point3D>() {
            @Override
            public int compare(Point3D o1, Point3D o2) {
                return PointsHandler.compare(o1, o2, 2);
            }
        };
    }

    public static int compare(Point3D p1, Point3D p2, int dimension){
        if(dimension == 0){
            return compare(p1.getX(), p1.getY(), p1.getZ(), p2.getX(), p2.getY(), p2.getZ());
        }
        if(dimension == 1){
            return compare(p1.getY(), p1.getZ(), p1.getX(), p2.getY(), p2.getZ(), p2.getX());
        }
        if(dimension == 2){
            return compare(p1.getZ(), p1.getX(), p1.getY(), p2.getZ(), p2.getX(), p2.getY());
        }
        return -1;
    }

    public static int compare(double p1, double p2, double p3, double q1, double q2, double q3){
        int i = Double.compare(p1, q1);
        if(i != 0) return i;

        i = Double.compare(p2, q2);
        if(i != 0) return i;

        return Double.compare(p3, q3);
    }

    public void sort(List<Point3D> points, int dimension){
        if(dimension%3 == 0){
            Collections.sort(points, compareX);
        }
        if(dimension%3 == 1){
            Collections.sort(points, compareY);
        }
        if(dimension%3 == 2){
            Collections.sort(points, compareZ);
        }
    }

    public List<Point3D> transformSegmentToPoint(List<Segment> segments){
        List<Point3D> points = new ArrayList<>();
        for(Segment s : segments){
            points.add(new Point3D(s.getX1(), s.getX2(), s.getY()));
        }
        return points;
    }

    public List<Segment> transformPointToSegment(List<Point3D> points){
        List<Segment> segments = new ArrayList<>();
        for(Point3D p : points){
            segments.add(new Segment(p.getZ(), p.getX(), p.getY(), true));
        }
        return segments;
    }
}
